from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from .models import Project
from .forms import ProjectForm


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    return render(
        request, "projects/list_projects.html", {"projects": projects}
    )


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST, user=request.user)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm(user=request.user)
    return render(request, "projects/create_project.html", {"form": form})


def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {"project": project}
    return render(request, "projects/show_project.html", context)
