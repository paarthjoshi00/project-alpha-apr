from django import forms
from .models import Project


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ["name", "description", "owner"]
        widgets = {"owner": forms.HiddenInput()}

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user", None)
        super().__init__(*args, **kwargs)
        if self.user:
            self.initial["owner"] = self.user.id

    def save(self, commit=True):
        instance = super().save(commit=False)
        if not instance.pk:
            instance.owner = self.user
        if commit:
            instance.save()
        return instance
