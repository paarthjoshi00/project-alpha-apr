from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from accounts.forms import LoginForm, UserCreationForm
from django.contrib.auth.models import User


def signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password2"]
            first_name = form.cleaned_data["first_name"]
            last_name = form.cleaned_data["last_name"]

            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password=password,
                    first_name=first_name,
                    last_name=last_name,
                )
                login(request, user)
                return redirect("list_projects")
            else:
                form.add_error(
                    "password_confirmation", "The passwords do not match"
                )
    else:
        form = UserCreationForm()

    context = {"form": form}
    return render(request, "registration/signup.html", context)


def user_login(request):
    if request.method == "POST":
        form = LoginForm(request, request.POST)
        if form.is_valid():
            user = authenticate(
                username=form.cleaned_data["username"],
                password=form.cleaned_data["password"],
            )
            if user is not None:
                login(request, user)
                return redirect("list_projects")
            else:
                form.add_error(None, "Invalid username or password.")
    else:
        form = LoginForm()
    context = {"form": form}
    return render(request, "registration/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")
